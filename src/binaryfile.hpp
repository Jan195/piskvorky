#ifndef BINARYFILE_HPP
#define BINARYFILE_HPP

#include <QDataStream>

#include "file.hpp"

class BinaryFile : public File, protected QDataStream
{
public:
    explicit BinaryFile(const QString &path = "") : File(path) {
        this->setVersion(QDataStream::Qt_5_6);
    }

protected:
    virtual void setDevice(QIODevice *device) final {
        QDataStream::setDevice(device);
    }
};

#endif // BINARYFILE_HPP
