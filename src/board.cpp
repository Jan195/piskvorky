#include <algorithm>

#include "board.hpp"

namespace {
int minimize(int num) {
    if (num > 0)
        num = 1;
    else if (num < 0)
        num = -1;
    return num;
}
int increment(int num) {
    if (num > 0)
        num++;
    else if (num < 0)
        num--;
    return num;
}
}

void Board::Line::minimize() {
    offset.x = ::minimize(offset.x);
    offset.y = ::minimize(offset.y);
}

void Board::Line::increment() {
    offset.x = ::increment(offset.x);
    offset.y = ::increment(offset.y);
}

void Board::clearBoard(const bd::Size size) {
    field.resize(size.width);
    for (int x = 0; x < size.width; ++x) {
        field[x].resize(size.height);
        for (int y = 0; y < size.height; ++y)
            field[x][y] = Player::NONE;
    }
}

bool Board::isCellExist(const bd::Point position) const {
    if (position.x < getSize().width && position.x >= 0 &&
            position.y < getSize().height && position.y >= 0)
        return true;
    else return false;
}

Player Board::getValue(const bd::Point position) const {
    if (isCellExist(position))
        return field[position.x][position.y];
    else
        throw std::out_of_range("Invalid game board position at " + bd::toString(position));
}

void Board::setValue(const bd::Point position, Player player) {
    if (isCellExist(position) && field[position.x][position.y] == Player::NONE)
        field[position.x][position.y] = player;
    else
        throw invalid_turn("Invalid turn at position " + bd::toString(position));
}

bool Board::isWinner(Player player) {
    return findWinningChain({4, 0}, player) || findWinningChain({0, 4}, player) ||
            findWinningChain({4, 4}, player) || findWinningChain({4, -4}, player);
}

bool Board::isFull() const {
    for (int x = 0; x < getSize().width; ++x)
        for (int y = 0; y < getSize().height; ++y)
            if (Player::NONE == getValue({x,y}))
                return false;
    return true;
}

bool Board::findWinningChain(bd::Point range, Player player)
{
    for (int x = 0; x < getSize().width; ++x) {
        for (int y = 0; y < getSize().height; ++y) {
            Line line{{x, y}, range};
            std::vector<Player> chain = getChain(line);
            if (chain.size() == WINNING_CHAIN_LENGHT
                    && isChainWinning(chain, player)) {
                if (getStoneBefore(line) != player && getStoneAtfter(line) != player) {
                    winLine = {line.polar, line.getEnd()};
                    return true;
                }
            }
        }
    }
    return false;
}

std::vector<Player> Board::getChain(Line line) const {
    std::vector<Player> chain;
    line.increment();
    const bd::Point finalOffset = line.offset;
    chain.push_back(getValue(line.polar));
    line.minimize();
    while (line.offset != finalOffset) {
        if (isCellExist(line.getEnd()))
            chain.push_back(getValue(line.getEnd()));
        else
            break;
        line.increment();
    }
    return chain;
}

bool Board::isChainWinning(const std::vector<Player> &rChain, const Player player) const {
    return std::all_of(rChain.cbegin(), rChain.cend(), [&](Player sign) { return sign == player; });
}

Player Board::getStoneAtfter(Line line) const {
    line.increment();
    if (isCellExist(line.getEnd()))
        return getValue(line.getEnd());
    else
        return Player::NONE;
}

Player Board::getStoneBefore(Line line) const {
    line = {line.polar, -line.offset};
    line.minimize();
    if (isCellExist(line.getEnd()))
        return getValue(line.getEnd());
    else
        return Player::NONE;
}
