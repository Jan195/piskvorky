#include <QApplication>

#include "config/appconfig.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    AppConfig appConfig("data/config.txt");
    QObject::connect(&app, &QApplication::aboutToQuit, &appConfig, &AppConfig::save);
    appConfig.updateGame();
    appConfig.start();

    return app.exec();
}
