#ifndef GAMERESULT_HPP
#define GAMERESULT_HPP

enum class GameResult : char { NONE = 0,
                               PLAYER1 = 'x',
                               PLAYER2 = 'o',
                               DRAW = 3 };

#endif // GAMERESULT_HPP
