﻿#include "gwidget.hpp"

bd::Point GWidget::getCellPosition(bd::Point cursorPosition) {
    cursorPosition /= cellSize;
    return cursorPosition;
}

bool GWidget::isWithinBoard(bd::Point point) {
    if (observedBoard->getSize().isWithin(point))
        return true;
    else
        return false;
}

void GWidget::paintEvent(QPaintEvent *e) {
    painter.begin(this);
    if (!backgroundOrigin.isNull()) painter.drawImage(0, 0, background);
    paintGrid();
    paintStones();
    if (bd::isValid(observedBoard->getWinLine()))
        paintWinningLine();
    painter.end();
}

void GWidget::paintGrid() {
    for (int x = 0; x < observedBoard->getSize().width + 1; ++x)
        painter.drawLine(cellSize * x, 0,
                    cellSize * x, cellSize * observedBoard->getSize().height);
    for (int y = 0; y < observedBoard->getSize().height + 1; ++y)
        painter.drawLine(0, cellSize * y, cellSize * observedBoard->getSize().width, cellSize * y);
}

void GWidget::paintStones() {
    for (int x = 0; x < observedBoard->getSize().width; ++x) {
        for (int y = 0; y < observedBoard->getSize().height; ++y) {
            if (observedBoard->getValue({x,y}) != Player::NONE)
                painter.drawImage(cellSize * x, cellSize * y, getStoneImage({x,y}));
        }
    }
}

void GWidget::paintWinningLine() {
    QPen winLinePen = painter.pen();
    winLinePen.setWidth(3);
    painter.setPen(winLinePen);
    bd::Line winLine = observedBoard->getWinLine() * cellSize + (cellSize / 2);
    painter.drawLine(winLine.start.x, winLine.start.y,
                winLine.end.x, winLine.end.y);
}

const QImage& GWidget::getStoneImage(bd::Point position) const {
    if (lastStonePosition == position) {
        if (observedBoard->getValue(position) == Player::PLAYER1)
            return player1Stone.last;
        else
            return player2Stone.last;
    }
    else {
        if (observedBoard->getValue(position) == Player::PLAYER1)
            return player1Stone.main;
        else
            return player2Stone.main;
    }
}

void GWidget::resizeEvent(QResizeEvent *event) {
    resizeImages();
    update();
}

void GWidget::loadOriginImages(const ImagesPaths &config) {
    player1Stone.origin.load(config.stone1.stone);
    player1Stone.originLast.load(config.stone1.stoneLast);
    player2Stone.origin.load(config.stone2.stone);
    player2Stone.originLast.load(config.stone2.stoneLast);
    if (!config.backgroundPath.isEmpty())
        backgroundOrigin.load(config.backgroundPath);
    else
        backgroundOrigin = QImage();
    resizeImages();
}

void GWidget::resizeImages() {
    bd::Size size = {this->width() - 2,this->height() - 2};

    if (!backgroundOrigin.isNull())
        background = backgroundOrigin.scaled(size.width, size.height);
    updateCellSize(size);
    player1Stone.scale(cellSize);
    player2Stone.scale(cellSize);
}

void GWidget::updateCellSize(bd::Size size) {
    if (size.width / observedBoard->getSize().width
            <= size.height / observedBoard->getSize().height)
        cellSize = size.width / observedBoard->getSize().width;
    else
        cellSize = size.height / observedBoard->getSize().height;
}

void GWidget::PlayerStone::scale(int toSize) {
    main = origin.scaledToHeight(toSize, Qt::SmoothTransformation);
    last = originLast.scaledToHeight(toSize, Qt::SmoothTransformation);
}
