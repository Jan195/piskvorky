#ifndef ARRAYS_HPP
#define ARRAYS_HPP

#include <vector>
#include <stdexcept>

#include "bidimensional.hpp"
#include "player.hpp"

class invalid_turn : public std::runtime_error {
public:
    explicit invalid_turn(const std::string& whatArg) : std::runtime_error(whatArg) {}
};

class Board
{
public:
    explicit Board(const bd::Size size) {
        clearBoard(size);
    }
    void clearBoard(const bd::Size size);
    bool isCellExist(const bd::Point) const;
    bool isWinner(Player player);
    bool isFull() const;
    bd::Size getSize() const {
        return {static_cast<int>(field.size()), static_cast<int>(field[0].size())};
    }
    bd::Line getWinLine() const { return winLine; }
    Player getValue(const bd::Point position) const;
    void setValue(const bd::Point position, const Player player);

private:
    struct Line {
        bd::Point polar, offset;

        bd::Point getEnd() {
            return polar + offset;
        }
        void minimize();
        void increment();
        bd::Point getOpposite() const;
    };
    const unsigned int WINNING_CHAIN_LENGHT = 5;
    bool findWinningChain(bd::Point range, Player player);
    std::vector<Player> getChain(Line line) const;
    bool isChainWinning(const std::vector<Player> &rChain, const Player player) const;
    Player getStoneAtfter(Line line) const;
    Player getStoneBefore(Line line) const;

    bd::Line winLine; /**< Line for highlighting winning chain*/
    std::vector<std::vector<Player>> field;
};

#endif // ARRAYS_H
