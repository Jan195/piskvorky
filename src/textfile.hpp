#ifndef TEXTFILE_HPP
#define TEXTFILE_HPP

#include <QTextStream>

#include "file.hpp"

class TextFile : public File, protected QTextStream
{
public:
    explicit TextFile(const QString &path = "") : File(path) {
        this->setCodec("utf-8");
    }

protected:
    virtual void setDevice(QIODevice *device) final {
        QTextStream::setDevice(device);
    }
};

#endif // TEXTFILE_HPP
