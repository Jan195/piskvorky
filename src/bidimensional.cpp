#include "bidimensional.hpp"

namespace bd {

Point::Point(const Size &s) {
    x = s.width;
    y = s.height;
}

Point& Point::operator+=(const Point &add) {
    x += add.x;
    y += add.y;
    return *this;
}

Point& Point::operator-=(const Point &sub) {
    x -= sub.x;
    y -= sub.y;
    return *this;
}

Point& Point::operator*=(int factor) {
    x *= factor;
    y *= factor;
    return *this;
}

Point& Point::operator/=(int divisor) {
    x /= divisor;
    y /= divisor;
    return *this;
}

Size& Size::operator+=(const Size &add) {
    width += add.width;
    height += add.height;
    return *this;
}

Size& Size::operator-=(const Size &sub) {
    width -= sub.width;
    height -= sub.height;
    return *this;
}

Size& Size::operator*=(int factor) {
    width *= factor;
    height *= factor;
    return *this;
}

Size& Size::operator/=(int divisor) {
    width /= divisor;
    height /= divisor;
    return *this;
}

bool Size::isWithin(bd::Point p) {
    if (p.x < width && p.y < height)
        return true;
    else
        return false;
}

Line& Line::operator+=(const Line &add) {
    start += add.start;
    end += add.end;
    return *this;
}

Line& Line::operator-=(const Line &sub) {
    start -= sub.start;
    end -= sub.end;
    return *this;
}

Line& Line::operator*=(int factor) {
    start *= factor;
    end *= factor;
    return *this;
}

Line& Line::operator/=(int divisor) {
    start /= divisor;
    end /= divisor;
    return *this;
}

bool operator==(const Point &p1, const Point &p2) {
    if (p1.x == p2.x && p1.y == p2.y)
        return true;
    else
        return false;
}

bool operator!=(const Point &p1, const Point &p2) {
    if (p1.x != p2.x || p1.y != p2.y)
        return true;
    else
        return false;
}

bool operator==(const Line &l1, const Line &l2) {
    if (l1.start == l2.start && l1.end == l2.end)
        return true;
    else
        return false;
}

bool operator!=(const Line &l1, const Line &l2) {
    if (l1.start != l2.start || l1.end != l2.end)
        return true;
    else
        return false;
}

bool isNull(const Point &p) {
    if (p == Point{0,0})
        return true;
    else
        return false;
}

bool isNull(const Line &l) {
    if (isNull(l.start) && isNull(l.end))
        return true;
    else
        return false;
}

bool isValid(const Line &l) {
    if (l.start != l.end)
        return true;
    else
        return false;
}

} // end of bd namespace
