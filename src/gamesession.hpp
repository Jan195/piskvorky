﻿#ifndef GAMESESSION_HPP
#define GAMESESSION_HPP

#include <QObject>
#include <memory>

#include "artificialintelligence.hpp"
#include "config/gameconfig.hpp"
#include "gameresult.hpp"

class GameSession : public QObject
{
    Q_OBJECT

public:
    GameSession() : GameSession(GameConfigBuilder().setDefault().get()) {}
    explicit GameSession(const GameConfig &config);

    void startNew();
    void play(bd::Point position = {0, 0});
    void resetScores() { player1.setScore(0); player2.setScore(0); }
    void configure(const GameConfig&);

    const Board* getBoard() const {
        return board.get();
    }
    Player getCurrentPlayer() const {
        return currentPlayer;
    }
    bd::Point getLastPosition() const {
        return lastPosition;
    }
    GameResult getResult() const {
        return result;
    }
    QPair<TypeOfPlayer, TypeOfPlayer> getTypeOfPlayer() const {
        return {player1.getType(), player2.getType()};
    }
    Score getScore() const {
        return {player1.getScore(), player2.getScore()};
    }
    int getTurnCount() const {
        return turnCount;
    }

signals:
    void boardChanged(const Board *board);

private:
    void clearBoard(bd::Size size);
    void setTypesOfPlayers(TypeOfPlayer p1, TypeOfPlayer p2);
    void addStone(bd::Point position);
    void controlWin();
    bool isCurrentPlayerHuman();
    bd::Point getAiTurn();
    void switchActivePlayer();
    void updateBoardForAiPlayers();

    class PlayerEntity {
    public:
        explicit PlayerEntity(Player p) : player(p), type(TypeOfPlayer::HUMAN), score(0) {}
        void changeType(TypeOfPlayer type);
        void updateBoardforAi(const Board *board) {
            ai->setObservedBoard(board);
        }
        void setScore(unsigned value) {
            score = value;
        }

        bool isHuman() const {
            if (type == TypeOfPlayer::HUMAN)
                return true;
            else
                return false;
        }
        TypeOfPlayer getType() const {
            return type;
        }
        unsigned getScore() const {
            return score;
        }
        bd::Point getTurn() {
            return ai->getTurn(player);
        }

    private:
        Player player;
        TypeOfPlayer type;
        std::unique_ptr<ArtificialIntelligence> ai;
        unsigned score;
    } player1, player2;

    std::unique_ptr<Board> board;
    bd::Point lastPosition;
    Player currentPlayer;
    GameResult result;
    int turnCount;
};

#endif // GAMESESSION_HPP
