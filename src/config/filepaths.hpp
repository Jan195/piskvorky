#ifndef PATHCONFIG_HPP
#define PATHCONFIG_HPP

#include <QString>

#include "textfile.hpp"

struct FilePaths
{
public:
    QString window, game, images, stoneCollection, languages;
};

class PathsConfigFile : public TextFile
{
public:
    explicit PathsConfigFile(const QString &path = "") : TextFile(path) {}

    const FilePaths& getPaths() {
        return paths;
    }

protected:
    virtual void loadContent() override;
    virtual void saveContent() override;

private:
    FilePaths paths;
};

#endif // PATHCONFIG_HPP
