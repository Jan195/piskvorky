#include <QMap>

#include "gameconfig.hpp"
#include "gamesession.hpp"

GameConfigBuilder& GameConfigBuilder::setDefault() {
    config.boardSize = {15, 15};
    config.playerTypes = {0, 0};
    config.score = {0, 0};
    return *this;
}

GameConfigBuilder& GameConfigBuilder::setStateFrom(const GameSession &gs) {
    config.boardSize = gs.getBoard()->getSize();
    config.playerTypes = {static_cast<int>(gs.getTypeOfPlayer().first),
                          static_cast<int>(gs.getTypeOfPlayer().second)};
    config.score = gs.getScore();
    return *this;
}

GameConfigBuilder& GameConfigBuilder::setBoardSizeAndPlayerTypes(const GameConfig &gc) {
    config.boardSize = gc.boardSize;
    config.playerTypes = gc.playerTypes;
    return *this;
}

void GameConfigBuilderFile::loadContent() {
    GameConfig config;
    QMap<qint8, int> map;
    *this >> map;
    config.boardSize = {map[0], map[1]};
    config.playerTypes = {map[2], map[3]};
    config.score = {static_cast<unsigned>(map[4]), static_cast<unsigned>(map[5])};
    set(config);
}

void GameConfigBuilderFile::saveContent() {
    const GameConfig config = get();
    QMap<qint8, int> map;
    map.insert(0, config.boardSize.width);
    map.insert(1, config.boardSize.height);
    map.insert(2, config.playerTypes.first);
    map.insert(3, config.playerTypes.second);
    map.insert(4, config.score.player1);
    map.insert(5, config.score.player2);
    *this << map;
}
