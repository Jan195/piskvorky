#ifndef APPCONFIG_HPP
#define APPCONFIG_HPP

#include "ui/mainwindow.hpp"
#include "ui/about.hpp"
#include "ui/settings.hpp"
#include "config/languages.hpp"
#include "config/filepaths.hpp"

class AppConfig : public QObject
{
    Q_OBJECT
public:
    explicit AppConfig(const QString &pathsConfigFilePath = "");
    virtual ~AppConfig() override {
        closeSettings();
    }

    void start();
    void updateGame();

public slots:
    void save();

private slots:
    void openAbout();
    void closeAbout() {

    }
    void openSettings();
    void saveSettings();
    void closeSettings() {
        settings.reset();
    }

private:
    void configureSettings();
    void loadFiles();
    void loadPaths(const QString&);
    void loadAndConfigureStones();
    void saveStones();
    void saveFiles();
    bool hasBoardSizeChanged(bd::Size oldSize);

    std::unique_ptr<Settings> settings;
    MainWindow win;
    About about;
    WindowConfigFile winConfig;
    GameConfigBuilderFile gameConfig;
    ImagesPathsBuilderFile imagesPaths;
    LanguagesBuilderFile languagesConfig;
    FilePaths paths;
    Translator translator;
};

#endif // APPCONFIG_HPP
