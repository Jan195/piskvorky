#include <QMap>

#include "windowconfig.hpp"

void WindowConfig::loadDefaultValues() {
    size = {602, 650};
}

void WindowConfigFile::loadContent() {
    QMap<qint8, unsigned int> map;
    *this >> map;
    setSize({static_cast<int>(map[0]), static_cast<int>(map[1])});
}

void WindowConfigFile::saveContent() {
    QMap<qint8, unsigned int> map;
    map.insert(0, getSize().width);
    map.insert(1, getSize().height);
    *this << map;
}
