#include "appconfig.hpp"

AppConfig::AppConfig(const QString &pathsConfigFilePath) : about(&win)
{
    connect(&win, &MainWindow::settingsOpened, this, &AppConfig::openSettings);
    connect(&win, &MainWindow::aboutOpened, this, &AppConfig::openAbout);
    win.setWindowIcon(QIcon("data/img/xo.png"));

    qApp->installTranslator(&translator);
    loadPaths(pathsConfigFilePath);
    loadFiles();
}

void AppConfig::loadPaths(const QString &configPath) {
    PathsConfigFile pathsConfig(configPath);
    pathsConfig.load();
    paths = pathsConfig.getPaths();
}

void AppConfig::loadFiles() {
    languagesConfig.load(paths.languages);
    translator.replaceLanguages(languagesConfig.get());
    translator.updateLanguage();

    winConfig.load(paths.window);
    gameConfig.load(paths.game);
    imagesPaths.load(paths.images);
}

void AppConfig::start() {
    win.show();
    win.startNewGame();
}

void AppConfig::openAbout() {
    about.move(win.pos().x() + win.width()/3, win.pos().y() + win.height()/3);
    about.setVisible(true);
}

void AppConfig::openSettings() {
    if (settings)
        return;
    save();
    settings.reset(new Settings(&win));
    configureSettings();
}

void AppConfig::configureSettings() {
    connect(settings.get(), &Settings::saved, this, &AppConfig::saveSettings);
    connect(settings.get(), &Settings::closed, this, &AppConfig::closeSettings);
    connect(&win, &MainWindow::resized, settings.get(), &Settings::center);
    loadAndConfigureStones();
    settings->setCurrentLanguage(translator.getLanguages());
    settings->configure(gameConfig.get());
    settings->setPaths(imagesPaths.get());
    settings->show();
}

void AppConfig::loadAndConfigureStones() {
    StonesBuilderFile stones;
    stones.load(paths.stoneCollection);
    settings->configure(stones.get());
}

void AppConfig::save() {
    win.saveTo(winConfig);
    gameConfig.setStateFrom(win.getGameSession());
    saveFiles();
}

void AppConfig::saveSettings() {
    translator.setLanguage(settings->getCurrentLanguage());
    languagesConfig.setCurrent(settings->getCurrentLanguage());
    winConfig.setSize({win.width(), win.height()});

    bd::Size oldSize = gameConfig.get().boardSize;
    gameConfig.setBoardSizeAndPlayerTypes(settings->getGameConfig());
    imagesPaths.set(settings->getImagesPaths());

    saveStones();

    updateGame();
    saveFiles();
    if (hasBoardSizeChanged(oldSize))
        win.startNewGame();
}

void AppConfig::updateGame() {
    win.load(winConfig);
    win.configureGame(gameConfig.get());
    win.updateImages(imagesPaths.get());
    win.updateTranslation();
    about.retranslate();
}

void AppConfig::saveStones() {
    StonesBuilderFile stones;
    stones.set(settings->getStones());
    stones.save(paths.stoneCollection);
}

void AppConfig::saveFiles() {
    winConfig.save(paths.window);
    gameConfig.save(paths.game);
    imagesPaths.save(paths.images);
    languagesConfig.save(paths.languages);
}

bool AppConfig::hasBoardSizeChanged(bd::Size oldSize) {
    if (oldSize == gameConfig.get().boardSize)
        return false;
    return true;
}
