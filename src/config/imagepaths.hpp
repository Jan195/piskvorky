#ifndef IMAGEPATHS_HPP
#define IMAGEPATHS_HPP

#include "binaryfile.hpp"
#include "bidimensional.hpp"
#include "stonepath.hpp"

struct ImagesPaths
{
    StonePath stone1, stone2;
    QString backgroundPath;
};

class ImagesPathsBuilder
{
public:
    ImagesPathsBuilder() {
        setDefault();
    }

    void setDefault();
    ImagesPathsBuilder& set(const ImagesPaths &ic) {
        config = ic;
        return *this;
    }

    const ImagesPaths& get() const {
        return config;
    }

private:
    ImagesPaths config;
};

class ImagesPathsBuilderFile : public ImagesPathsBuilder, public BinaryFile
{
public:
    explicit ImagesPathsBuilderFile(const QString &path = "") : BinaryFile(path) {}

protected:
    virtual void loadContent() override;
    virtual void saveContent() override;
};

#endif // GRAPHICSCONFIG_HPP
