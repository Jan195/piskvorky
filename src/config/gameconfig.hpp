#ifndef GAMECONFIG_HPP
#define GAMECONFIG_HPP

#include "binaryfile.hpp"
#include "bidimensional.hpp"

struct Score {
    unsigned player1, player2;
};

class GameSession;

struct GameConfig
{
    bd::Size boardSize;
    QPair<int, int> playerTypes;
    Score score;
};

class GameConfigBuilder {
public:
    GameConfigBuilder() {
        setDefault();
    }

    GameConfigBuilder& setDefault();
    GameConfigBuilder& setStateFrom(const GameSession &);
    GameConfigBuilder& setBoardSizeAndPlayerTypes(const GameConfig&);
    GameConfigBuilder& set(const GameConfig &gc) {
        config = gc;
        return *this;
    }
    const GameConfig& get() {
        return config;
    }

private:
    GameConfig config;
};

class GameConfigBuilderFile : public GameConfigBuilder, public BinaryFile
{
public:
    explicit GameConfigBuilderFile(const QString &path = "") : BinaryFile(path) {}

protected:
    virtual void loadContent() override;
    virtual void saveContent() override;
};

#endif // GAMECONFIG_HPP
