#include "languages.hpp"

#include <iostream>

void Languages::setCurrent(unsigned id) {
    if (id < paths.size()) {
        current = id;
    }
}

QString Languages::getPath() const {
    try {
        return getCurentPath();
    } catch (const std::out_of_range &exception) {
        std::cerr << exception.what() << std::endl;
        return QString();
    }
}

void Translator::setLanguage(unsigned id) {
    languages.setCurrent(id);
    updateLanguage();
}

void LanguagesBuilderFile::loadContent() {
    unsigned int current = readLine().toUInt();
    while (!this->atEnd()) {
        languages.addLanguage(readLine());
    }
    languages.setCurrent(current);
}

void LanguagesBuilderFile::saveContent() {
    if (languages.size() == 0)
        return;
    *this << languages.getCurrentId() << "\n";
    for (const QString &langPath : languages)
        *this << langPath + "\n";
}
