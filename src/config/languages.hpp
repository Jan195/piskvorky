#ifndef TRANSLATOR_HPP
#define TRANSLATOR_HPP

#include <QTranslator>
#include <vector>

#include "textfile.hpp"

class Languages
{
public:
    Languages() : current(0) {}

    auto begin() const noexcept {
        return paths.begin();
    }
    auto end() const noexcept {
        return paths.end();
    }
    auto size() const noexcept {
        return paths.size();
    }

    void addLanguage(const QString &path) {
        paths.emplace_back(path);
    }
    void setCurrent(unsigned id);

    QString getPath() const;
    unsigned getCurrentId() const {
        return current;
    }

private:
    QString getCurentPath() const noexcept(false) {
        if (current >= paths.size())
            throw std::out_of_range("Language with index " + std::to_string(current)
                                    + " could not be found and loaded");
        else
            return paths[current];
    }

    unsigned current;
    std::vector<QString> paths;
};

class Translator : public QTranslator
{
public:
    void replaceLanguages(const Languages &langs) {
        languages = langs;
    }
    void setLanguage(unsigned id);
    void updateLanguage() {
        load(languages.getPath());
    }
    const Languages& getLanguages() const {
        return languages;
    }

private:
    Languages languages;
};

class LanguagesBuilderFile : public TextFile
{
public:
    explicit LanguagesBuilderFile(const QString &path = "") : TextFile(path) {}
    void setCurrent(unsigned id) {
        languages.setCurrent(id);
    }
    const Languages& get() {
        return languages;
    }

protected:
    virtual void loadContent() override;
    virtual void saveContent() override;

private:
    Languages languages;
};

#endif // LANGUESCONFIG_HPP
