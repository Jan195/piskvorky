#include <QMap>

#include "imagepaths.hpp"

void ImagesPathsBuilder::setDefault() {
    config.stone1 = {"data/img/xm.png", "data/img/xl.png"};
    config.stone2 = {"data/img/om.png", "data/img/ol.png"};
    config.backgroundPath.clear();
}

void ImagesPathsBuilderFile::loadContent() {
    ImagesPaths config;
    QMap<qint8, QString> map;
    *this >> map;
    config.stone1 = {map[0], map[1]};
    config.stone2 = {map[2], map[3]};
    config.backgroundPath = map[4];
    set(config);
}

void ImagesPathsBuilderFile::saveContent() {
    const ImagesPaths config = get();
    QMap<qint8, QString> map;
    map.insert(0, config.stone1.stone);
    map.insert(1, config.stone1.stoneLast);
    map.insert(2, config.stone2.stone);
    map.insert(3, config.stone2.stoneLast);
    map.insert(4, config.backgroundPath);
    *this << map;
}
