#ifndef WINDOWCONFIG_HPP
#define WINDOWCONFIG_HPP

#include "binaryfile.hpp"
#include "bidimensional.hpp"

class WindowConfig {
public:
    WindowConfig() {
        loadDefaultValues();
    }
    virtual ~WindowConfig() = default;

    void setSize(const bd::Size size) {
        this->size = size;
    }
    bd::Size getSize() const {
        return size;
    }

private:
    void loadDefaultValues();
    bd::Size size;
};

class WindowConfigFile : public WindowConfig, public BinaryFile
{
public:
    explicit WindowConfigFile(const QString &path = "") : BinaryFile(path) {}

protected:
    virtual void loadContent() override;
    virtual void saveContent() override;
};

#endif // WINDOWCONFIG_HPP
