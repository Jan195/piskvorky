#include "filepaths.hpp"

void PathsConfigFile::loadContent() {
    paths.window = this->readLine();
    paths.game = this->readLine();
    paths.images = this->readLine();
    paths.stoneCollection = this->readLine();
    paths.languages = this->readLine();
}

void PathsConfigFile::saveContent() {
    *this << paths.window + "\n"
            + paths.game + "\n"
            + paths.images + "\n"
            + paths.stoneCollection + "\n"
            + paths.languages;
}
