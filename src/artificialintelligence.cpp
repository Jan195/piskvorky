#include <iostream>
#include <time.h>

#include "artificialintelligence.hpp"

std::unique_ptr<ArtificialIntelligence> AiFactory::create(TypeOfPlayer aiType) {
    switch (aiType) {
        case TypeOfPlayer::AI_EASY:
            return std::make_unique<ArtificialIntelligenceEasy>(ArtificialIntelligenceEasy());
        case TypeOfPlayer::AI_MEDIUM:
            return std::make_unique<ArtificialIntelligenceNormal>(ArtificialIntelligenceNormal());
        case TypeOfPlayer::HUMAN:
            throw std::runtime_error("AI cannot be created");
    }
}

ArtificialIntelligence::ArtificialIntelligence() : observedBoard(nullptr) {
    random.seed(static_cast<unsigned long>(time(nullptr)));
}

void ArtificialIntelligence::setObservedBoard(const Board *board) {
    observedBoard = board;
    size = board->getSize();
    scoreTable.resize(size.width);
        for (unsigned int x = 0; x < scoreTable.size(); ++x)
            scoreTable[x].resize(size.height);
}

bd::Point ArtificialIntelligenceEasy::getTurn(Player aiPlayer) {
    initializeTurn();
    Player ai_player = aiPlayer,
            enemy_player = getEnemy(aiPlayer);
    for (int x = 0; x < getSize().width; ++x) {
        for (int y = 0; y < getSize().height; ++y) {
            if (*getValue({x, y}) == Player::NONE) {
                fillScore({x, y}, 1, 2, enemy_player);
                fillScore({x, y}, 1, 2, ai_player);
                fillScore({x, y}, 2, 4, enemy_player);
                fillScore({x, y}, 2, 5, ai_player);
                fillScore({x, y}, 3, 11, enemy_player);
                fillScore({x, y}, 3, 12, ai_player);
                fillScore({x, y}, 4, 23, enemy_player);
                fillScore({x, y}, 4, 25, ai_player);
                fillScore({x, y}, 5, -22, enemy_player);
                fillScore({x, y}, 5, -27, ai_player);
            }
        }
    }
    return evaluateFinalResult();
}

bd::Point ArtificialIntelligenceNormal::getTurn(Player aiPlayer) {
    initializeTurn();
    Player ai_player = aiPlayer,
            enemy_player = getEnemy(aiPlayer);
    for (int x = 0; x < getSize().width; ++x) {
        for (int y = 0; y < getSize().height; ++y) {
            if (*getValue({x, y}) == Player::NONE) {
                fillScore({x, y}, 1, 2, enemy_player);
                fillScore({x, y}, 1, 2, ai_player, true);
                fillScore({x, y}, 2, 4, enemy_player);
                fillScore({x, y}, 2, 5, ai_player);
                fillScore({x, y}, 3, 14, enemy_player);
                fillScore({x, y}, 3, 15, ai_player);
                fillScore({x, y}, 4, 37, enemy_player);
                fillScore({x, y}, 4, 39, ai_player);
                fillScore({x, y}, 5, -31, enemy_player);
                fillScore({x, y}, 5, -36, ai_player);
            }
        }
    }
    return evaluateFinalResult();
}

void ArtificialIntelligence::initializeTurn() {
    for (int x = 0; x < size.width; ++x) {
        for (int y = 0; y < size.height; ++y)
            scoreTable[x][y] = 0;
    }
}

std::optional<Player> ArtificialIntelligence::getValue(bd::Point p) const {
    if (observedBoard->isCellExist(p))
        return observedBoard->getValue(p);
    else
        return std::nullopt;
}

Player ArtificialIntelligence::getEnemy(Player aiPlayer) const {
    if (aiPlayer == Player::PLAYER1)
        return Player::PLAYER2;
    else
        return Player::PLAYER1;
}

void ArtificialIntelligence::fillScore(const bd::Point position, const int lenght, const int score,
                                       const Player player, const bool diagonalBonus) {
    const std::vector<Player> goal(lenght, player);
    int final_score = 0,
            border_penalization = getBoarderPenalization(position, lenght);
    lines resultLines = getSurroundings(position, lenght);

    for (unsigned int directionId = 0; directionId < resultLines.size(); ++directionId) {
        Direction direction = static_cast<Direction>(directionId);
        if (resultLines[direction] == goal) {
            const PolarLine line = {direction, position, lenght};
            int cell_score = score;
            if (isCellAvailable(getCellAtEnd(line)))
                cell_score += score;
            if (diagonalBonus && isDiagonal(direction))
                cell_score++;
            final_score += cell_score;
        }
    }
    final_score += border_penalization;
    scoreTable[position.x][position.y] += final_score;
}

int ArtificialIntelligence::getBoarderPenalization(const bd::Point position, int range) const {
    int border_score = 0;
    for (int r = 1; r <= range; ++r) {
        if (r < 4 && (position.x + r >= size.width ||
                      position.y + r >= size.height ||
                      position.x - r < 0 ||
                      position.y - r < 0))
            border_score = -1;
    }
    return border_score;
}

const ArtificialIntelligence::lines ArtificialIntelligence::getSurroundings(bd::Point pos,
                                                                              int range) const {
    lines result;
    for (int r = 1; r <= range; ++r) {
        std::optional<Player> value;
        for (int directionId = 0; directionId < 8; ++directionId) {
            auto direct = static_cast<Direction>(directionId);
            if ((value = getValue(getPositionOnBoard({direct, pos, r}))))
                result[direct].push_back(*value);
        }
    }
    return result;
}

bool ArtificialIntelligence::isCellAvailable(bd::Point p) {
    if (getValue(p).value_or(Player::PLAYER1) == Player::NONE)
        return true;
    else
        return false;
}

bool ArtificialIntelligence::isDiagonal(Direction direction) const {
    if (direction == Direction::LEFT_UP || direction == Direction::LEFT_DOWN ||
            direction == Direction::RIGHT_UP || direction == Direction::RIGHT_DOWN)
        return true;
    else return false;
}

bd::Point ArtificialIntelligence::getPositionOnBoard(const PolarLine &point) const {
    switch (point.direction) {
    case Direction::RIGHT:
        return {point.pole.x + point.lenght, point.pole.y};
    case Direction::RIGHT_DOWN:
        return {point.pole.x + point.lenght, point.pole.y + point.lenght};
    case Direction::DOWN:
        return {point.pole.x, point.pole.y + point.lenght};
    case Direction::LEFT:
        return {point.pole.x - point.lenght, point.pole.y};
    case Direction::UP:
        return {point.pole.x, point.pole.y - point.lenght};
    case Direction::LEFT_UP:
        return {point.pole.x - point.lenght, point.pole.y - point.lenght};
    case Direction::RIGHT_UP:
        return {point.pole.x + point.lenght, point.pole.y - point.lenght};
    case Direction::LEFT_DOWN:
        return {point.pole.x - point.lenght, point.pole.y + point.lenght};
    }
}

bd::Point ArtificialIntelligence::evaluateFinalResult() {
    showScore();
    std::vector<CellResult> best_results;
    best_results.push_back({-32768, {0, 0}});

    for (int x = 0; x < size.width; ++x) {
        for (int y = 0; y < size.height; ++y) {
            if (*getValue({x, y}) == Player::NONE) {
                if (scoreTable[x][y] > best_results[0].score) {
                    best_results.clear();
                    best_results.push_back({scoreTable[x][y], {x, y}});
                }
                else if (scoreTable[x][y] == best_results[0].score)
                    best_results.push_back({scoreTable[x][y], {x, y}});
            }
        }
    }
    if (best_results.size() > 1) {
        std::uniform_int_distribution<unsigned long> rdist (0, best_results.size() - 1);
        best_results[0] = best_results[rdist(random)];
    }
    std::cout << best_results[0].position.x << ':'<< best_results[0].position.y << std::endl;
    return best_results[0].position;
}

void ArtificialIntelligence::showScore() const {
    for (int y = 0; y < size.height; y++) {
        for (int x = 0; x < size.width; x++) {
            if (*getValue({x, y}) == Player::NONE) {
                if (scoreTable[x][y] < 10 && scoreTable[x][y] >= 0) std::cout << " ";
                std::cout << scoreTable[x][y] << ":";
            }
            else std::cout << " " << static_cast<char>(*getValue({x, y})) << ":";
        }
         std::cout << "\n";
    }
}
