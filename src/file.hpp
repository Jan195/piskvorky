#ifndef FILE_HPP
#define FILE_HPP

#include <QString>
#include <QIODevice>
#include <QFile>

class File
{
public:
    explicit File(const QString &path = "") : file(path) {}
    virtual ~File() = default;

    void load();
    void save();

    void load(const QString &path) {
        file.setFileName(path);
        load();
    }
    void save(const QString &path) {
        file.setFileName(path);
        save();
    }

protected:
    virtual void setDevice(QIODevice*) = 0;

    virtual void loadContent() = 0;
    virtual void saveContent() = 0;

private:
    QFile file;
};

#endif // FILE_HPP
