#include "gamesession.hpp"

GameSession::GameSession(const GameConfig &config) : player1(Player::PLAYER1), player2(Player::PLAYER2),
    currentPlayer(Player::PLAYER1), result(GameResult::NONE)
{
    configure(config);
}

void GameSession::startNew() {
    result = GameResult::NONE;
    currentPlayer = Player::PLAYER1;
    turnCount = 0;
    lastPosition = {0,0};
    clearBoard(board->getSize());
    if (!isCurrentPlayerHuman())
        play();
}

void GameSession::play(bd::Point position) {
    if (result != GameResult::NONE)
        return;
    else if (!isCurrentPlayerHuman())
        addStone(getAiTurn());
    else {
        addStone(position);
        if (!isCurrentPlayerHuman())
            play();
    }
}

bool GameSession::isCurrentPlayerHuman() {
    if ((currentPlayer == Player::PLAYER1 && player1.isHuman()) ||
            (currentPlayer == Player::PLAYER2 && player2.isHuman()))
        return true;
    else return false;
}

bd::Point GameSession::getAiTurn() {
    if (currentPlayer == Player::PLAYER1)
        return player1.getTurn();
    else
        return player2.getTurn();
}

void GameSession::switchActivePlayer() {
    if (currentPlayer == Player::PLAYER1)
        currentPlayer = Player::PLAYER2;
    else
        currentPlayer = Player::PLAYER1;
}

void GameSession::addStone(bd::Point position) {
    board->setValue(position, currentPlayer);
    lastPosition = position;
    ++turnCount;
    controlWin();
    switchActivePlayer();
}

void GameSession::controlWin() {
    if (board->isWinner(currentPlayer)) {
        result = static_cast<GameResult>(currentPlayer);
        if (currentPlayer == Player::PLAYER1)
            player1.setScore(player1.getScore() + 1);
        else
            player2.setScore(player2.getScore() + 1);
    }
    else if (board->isFull())
        result = GameResult::DRAW;
    else
        result = GameResult::NONE;
}

void GameSession::setTypesOfPlayers(TypeOfPlayer p1, TypeOfPlayer p2) {
    player1.changeType(p1);
    player2.changeType(p2);
    updateBoardForAiPlayers();
}

void GameSession::PlayerEntity::changeType(TypeOfPlayer type) {
    this->type = type;
    if (type == TypeOfPlayer::HUMAN)
        ai.reset();
    else
        ai = AiFactory::create(this->type);
}

void GameSession::clearBoard(bd::Size size) {
    board.reset(new Board(size));
    updateBoardForAiPlayers();
    emit boardChanged(board.get());
}

void GameSession::updateBoardForAiPlayers() {
    if (!player1.isHuman())
        player1.updateBoardforAi(board.get());
    if (!player2.isHuman())
        player2.updateBoardforAi(board.get());
}

void GameSession::configure(const GameConfig &gc) {
    if (!board || board->getSize() != gc.boardSize)
        clearBoard(gc.boardSize);
    setTypesOfPlayers(static_cast<TypeOfPlayer>(gc.playerTypes.first),
                         static_cast<TypeOfPlayer>(gc.playerTypes.second));
    player1.setScore(gc.score.player1);
    player2.setScore(gc.score.player2);
    if (!isCurrentPlayerHuman())
        play();
}
