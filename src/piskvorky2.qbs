import qbs

Project {
    minimumQbsVersion: "1.7.1"

    CppApplication {
        sourceDirectory: "src/"
        name: "Piskvorky2"
        version: "2.3"
        cpp.cxxLanguageVersion: ["c++17"]
        Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
        Depends { name: "vcs" }
        files: [
            "ui/*",
            "translations/*",
            "artificialintelligence.*",
            "bidimensional.*",
            "board.*",
            "gamesession.*",
            "gwidget.*",
            "main.cpp"
        ]
        Group {
            name: "Headers only"
            files: [
                "gameresult.hpp",
                "player.hpp",
                "stonepath.hpp",
            ]
        }
        Group {
            name: "Input/Output"
            files: [
                "config/*",
                "file.*",
                "binaryfile.*",
                "textfile.*",
                "stones.*"
            ]
        }
        cpp.includePaths: [path]

        Group {     // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install: true
        }
    }
    references: ["test/test.qbs"]
}
