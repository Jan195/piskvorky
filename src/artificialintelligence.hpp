#ifndef ARTIFICIALINTELLIGENCE_HPP
#define ARTIFICIALINTELLIGENCE_HPP

#include <random>
#include <memory>
#include <map>
#include <optional>

#include "board.hpp"

/**
 * @brief Distinction between types of players.
 */
enum class TypeOfPlayer : unsigned { HUMAN = 0,
                                     AI_EASY = 1,
                                     AI_MEDIUM = 2
                                   };

class ArtificialIntelligence
{
public:
    ArtificialIntelligence();
    virtual ~ArtificialIntelligence() = default;

    virtual void setObservedBoard(const Board *) final;
    virtual bd::Point getTurn(Player aiPlayer) = 0;

protected:
    enum struct Direction : unsigned int {
        UP = 0, RIGHT_UP = 1, RIGHT = 2, RIGHT_DOWN = 3,
        DOWN = 4, LEFT_DOWN = 5, LEFT = 6, LEFT_UP = 7
    };
    struct PolarLine {
        Direction direction;
        bd::Point pole;
        int lenght;

        PolarLine operator+(int i) const {
            return {direction, pole, lenght+i};
        }
    };
    struct CellResult {
        int score;
        bd::Point position;
    };
    using lines = std::map<Direction, std::vector<Player>>;

    bd::Size getSize() const {
        return size;
    }
    std::optional<Player> getValue(bd::Point p) const;
    virtual int getBoarderPenalization(bd::Point, int range) const;
    Player getEnemy(Player aiPlayer) const;

    bool isCellAvailable(bd::Point p);
    bool isDiagonal(Direction direction) const;

    void initializeTurn();
    const lines getSurroundings(bd::Point pos, int range) const;
    /**
     * @brief Filling the cell in score table.
     * @param position Position on the board
     * @param range Range for all directions
     * @param score How much is rated
     * @param player Current player
     * @param diagonalBonus Sometimes is more effective play diagonally
     */
    virtual void fillScore(const bd::Point position, const int range, const int score,
                           const Player player, const bool diagonalBonus = false);
    virtual bd::Point evaluateFinalResult() final;

private:
    bd::Point getCellAtEnd(const PolarLine& pl) const {
        return getPositionOnBoard(pl+1);
    }
    bd::Point getPositionOnBoard(const PolarLine&) const;
    [[deprecated]] void showScore() const;

    std::mt19937 random;
    const Board *observedBoard;
    std::vector<std::vector<int>> scoreTable;
    bd::Size size;
};

class ArtificialIntelligenceEasy : public ArtificialIntelligence
{
public:
    virtual bd::Point getTurn(Player aiPlayer);
};

class ArtificialIntelligenceNormal : public ArtificialIntelligence

{
public:
    virtual bd::Point getTurn(Player aiPlayer);
};

namespace AiFactory {
    std::unique_ptr<ArtificialIntelligence> create(TypeOfPlayer aiType);
}

#endif // ARTIFICIALINTELLIGENCE_HPP
