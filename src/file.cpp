#include <QFile>

#include "file.hpp"

void File::load() {
    if (file.open(QIODevice::ReadOnly)) {
        this->setDevice(&file);
        loadContent();
        file.close();
    }
}

void File::save() {
    if (file.open(QIODevice::WriteOnly)) {
        this->setDevice(&file);
        saveContent();
        file.close();
    }
}
