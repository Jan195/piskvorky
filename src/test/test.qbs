import qbs

CppApplication {
    name: "unitTests"
    type: ["application", "autotest"]
    consoleApplication: true
    cpp.cxxLanguageVersion: [ "c++17" ]
    Depends { name: "Qt"; submodules: ["testlib"] }
    cpp.includePaths: [
        "/usr/include/unit-testing",
        "../"
    ]
    files: [
        "bidimensionaltests.cpp",
        "configs.cpp",
        "game.cpp",
        "main.cpp",
        "testCases1.cpp",
    ]
    Group {
        name: "App moc files"
        prefix: "../"
        Qt.core.generatedHeadersDir: product.buildDirectory + "/qt.header2"
        files: ["gamesession.*"]
    }
    Group {
        name: "App translation units"
        prefix: "../"
        files: [
            "board.cpp",
            "artificialintelligence.cpp",
            "bidimensional.cpp",
            "config/gameconfig.cpp",
            "config/graphicsconfig.cpp"
        ]
    }

    Group { // Properties for the produced executable
        fileTagsFilter: ["application", "autotest"]
        qbs.install: true
        qbs.installDir: "test"
    }
}
