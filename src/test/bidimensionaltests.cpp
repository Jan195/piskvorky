#include <catch.hpp>
#include <trompeloeil.hpp>

#include "bidimensional.hpp"

using namespace bd;

SCENARIO("Test of 2D entities", "[bd]")
{
    GIVEN("One point") {
        Point point1{10,10};
        WHEN("Set position by accessing to data members") {
            point1.y = 15;
            point1.x = 15;
            THEN("Point's position changes") {
                REQUIRE(point1 == Point{15,15});
            }
        }
        WHEN("Construct new point") {
            Point point2{5,5};
            THEN("Point's position is correct") {
                REQUIRE(point2 == Point{5,5});
                AND_THEN("Point's position is not null")
                    REQUIRE_FALSE(isNull(point2));
            }
        }
        WHEN("Construct new point by implicit constructor") {
            Point point2;
            THEN("Point's position is implicit") {
                REQUIRE(point2 == Point{0,0});
                AND_THEN("Point's position is also null")
                    REQUIRE(isNull(point2));
            }
        }
        WHEN("Assign new position to the point") {
            point1 = {5,5};
            THEN("Point's position changes")
                REQUIRE(point1 == Point{5,5});
        }
        WHEN("Add up points") {
            point1 += {5,5};
            THEN("Point's position changes") {
                REQUIRE(point1 == Point{15,15});
                REQUIRE((point1 + Point{5,5}) == Point{20,20});
            }
        }
        WHEN("Subtract points") {
            point1 -= {5,5};
            THEN("Point's position changes") {
                REQUIRE(point1 == Point{5,5});
                REQUIRE((point1 - Point{4,4}) == Point{1,1});
            }
        }
        WHEN("Negative point") {
            point1 = -point1;
            THEN("Point's position changes")
                REQUIRE(point1 == Point{-10,-10});
        }
        WHEN("Multiply points") {
            point1 *= 5;
            THEN("Point's position changes") {
                REQUIRE(point1 == Point{50,50});
                REQUIRE((point1 * 2) == Point{100,100});
            }
        }
        WHEN("Points division") {
            point1 /= 2;
            THEN("Point's position changes") {
                REQUIRE(point1 == Point{5,5});
                REQUIRE((point1 / 2) == Point{2,2});
            }
        }
        WHEN("Comparative points among themselves") {
            REQUIRE(point1 == Point{10,10});
            REQUIRE(point1 != Point{10,5});
            REQUIRE(Point{10,10} != Point{5,10});
        }
    }
    GIVEN("One size") {
        Size size1{10,10};
        WHEN("Chnage size by accessing to data members") {
            size1.width = 15;
            size1.height = 15;
            THEN("Size changes") {
                REQUIRE(size1 == Size{15,15});
            }
        }
        WHEN("Construct new size") {
            Size size2{5,5};
            THEN("Size is correct") {
                REQUIRE(size2 == Size{5,5});
                AND_THEN("Size is not null")
                    REQUIRE_FALSE(isNull(size2));
            }
        }
        WHEN("Construct new implicit size") {
            Size size2;
            THEN("Size is implicit") {
                REQUIRE(size2 == Size{0,0});
                AND_THEN("Size is also null")
                    REQUIRE(isNull(size2));
            }
        }
        WHEN("Assign new size") {
            size1 = {5,5};
            THEN("Size changes")
                REQUIRE(size1 == Size{5,5});
        }
        WHEN("Add up sizes") {
            size1 += Size{5,5};
            THEN("Size changes") {
                REQUIRE(size1 == Size{15,15});
                REQUIRE((size1 + Size{5,5}) == Size{20,20});
            }
        }
        WHEN("Subtract sizes") {
            size1 -= {5,5};
            THEN("Size changes") {
                REQUIRE(size1 == Size{5,5});
                REQUIRE((size1 - Size{4,4}) == Size{1,1});
            }
        }
        WHEN("Negative size") {
            size1 = -size1;
            THEN("Size changes")
                REQUIRE(size1 == Size{-10,-10});
        }
        WHEN("Multiply size") {
            size1 *= 5;
            THEN("Size changes") {
                REQUIRE(size1 == Size{50,50});
                REQUIRE((size1 * 2) == Size{100,100});
            }
        }
        WHEN("Size division") {
            size1 /= 2;
            THEN("Size changes") {
                REQUIRE(size1 == Size{5,5});
                REQUIRE((size1 / 2) == Size{2,2});
            }
        }
        WHEN("Comparative sizes among themselves") {
            REQUIRE(size1 == Size{10,10});
            REQUIRE(size1 != Size{10,5});
            REQUIRE(Size{10,10} != Size{5,10});
        }
        WHEN("Find if point is within") {
            REQUIRE(size1.isWithin({5, 9}));
            REQUIRE_FALSE(size1.isWithin({10, 5}));
            REQUIRE_FALSE(size1.isWithin({5, 10}));
        }
    }
    GIVEN("One line") {
        Line line1{{10,10},{20,10}};
        WHEN("Construct new line") {
            Line line2{{10,10}, {20,20}};
            THEN("Positions are correct") {
                REQUIRE((line2.start == Point{10,10} && line2.end == Point{20,20}));
                AND_THEN("Line is not null")
                    REQUIRE_FALSE(isNull(line2));
            }
            AND_WHEN("Comparitive lines among themselves") {
                REQUIRE(line1 == Line{{10,10}, {20,10}});
                REQUIRE(Line{{10,10}, {10,10}} != Line{{10,10}, {20,10}});
            }
            AND_WHEN("Assign new line") {
                line1 = {{5,5},{15,15}};
                THEN("Position of line change")
                    REQUIRE(line1 == Line{{5,5},{15,15}});
            }
            AND_WHEN("Add up lines") {
                line1 += line2;
                THEN("Positions of lines are change") {
                    REQUIRE(line1 == Line{{20,20},{40,30}});
                    REQUIRE(line1 + 10 == Line{{30,30},{50,40}});
                    REQUIRE(line1 + line2 == Line{{30,30},{60,50}});
                }
            }
            AND_WHEN("Subtract lines") {
                line2 -= line1;
                THEN("Positions of lines are change") {
                    REQUIRE(line2 == Line{{0,0},{0,10}});
                    REQUIRE(line2 - 10 == Line{{-10,-10},{-10,0}});
                    REQUIRE(line2 - Line{{100,0},{50,100}} == Line{{-100,0},{-50,-90}});
                }
            }
            AND_WHEN("Negative line") {
                line1 = -line1;
                THEN("Positions of lines are change")
                    REQUIRE(line1 == Line{{-10,-10},{-20,-10}});
            }
            AND_WHEN("Multiply lines") {
                line1 *= 5;
                THEN("Positions of lines are change") {
                    REQUIRE(line1 == Line{{50,50},{100,50}});
                    REQUIRE(line1 * 2 == Line{{100,100},{200,100}});
                }
            }
            AND_WHEN("Line division") {
                line1 /= 5;
                THEN("Positions of lines are change") {
                    REQUIRE(line1 == Line{{2,2},{4,2}});
                    REQUIRE(line1 / 2 == Line{{1,1},{2,1}});
                }
            }
        }
        WHEN("Construct new implicit line") {
            Line line2;
            THEN("Poisitions of line are implicit") {
                REQUIRE(line2 == Line{{0,0}, {0,0}});
                AND_THEN("Line is also null and is not valid") {
                    REQUIRE(isNull(line2));
                    REQUIRE_FALSE(isValid(line2));
                }
            }
        }
        WHEN("Set same position at the start and also at the end of the line") {
            line1 = {{5,5},{5,5}};
            THEN("Line is not valid")
                REQUIRE_FALSE(isValid(line1));
        }
    }
}
