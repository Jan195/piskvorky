<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>About</name>
    <message>
        <location filename="../ui/about.ui" line="32"/>
        <source>About Piskvorky</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../ui/about.ui" line="57"/>
        <source>Piskvorky 2.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/about.ui" line="87"/>
        <source>Description</source>
        <translation>Piskvorky je typ hry známé také jako &apos;pět v řadě&apos; nebo &apos;gomoku&apos;&lt;br&gt;
Aplikaci vytvořil Jan Vondráček &lt;a href=&quot;mailto:jan195@null.com&quot;&gt;email&lt;/a&gt;&lt;br&gt;
Zdrojový kód na &lt;a href=&quot;https://gitlab.com/Jan195/piskvorky&quot;&gt;gitlabu&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>EditStoneDialog</name>
    <message>
        <location filename="../ui/editstonedialog.ui" line="32"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.ui" line="52"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.ui" line="62"/>
        <source>Image</source>
        <translation>Obrázek</translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.ui" line="85"/>
        <source>Highlight image</source>
        <translation>Zvýrazněný obrázek</translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.ui" line="140"/>
        <source>&amp;Delete</source>
        <translation>&amp;Smazat</translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.cpp" line="44"/>
        <source>Image Files(*.png *.jpg)</source>
        <translation>Obrazové soubory(*.png *.jpg)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.ui" line="26"/>
        <source>Piskvorky 2.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="64"/>
        <source>&amp;Game</source>
        <translation>&amp;Hra</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="71"/>
        <source>&amp;Options</source>
        <translation>&amp;Možnosti</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="82"/>
        <source>&amp;New game</source>
        <translation>&amp;Nová hra</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="87"/>
        <source>&amp;Settings</source>
        <translation>&amp;Nastavení</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="92"/>
        <source>&amp;Reset score</source>
        <translation>&amp;Resetuj skóre</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="97"/>
        <source>&amp;Quit app</source>
        <translation>&amp;Ukončit aplikaci</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="102"/>
        <source>&amp;About</source>
        <translation>&amp;O aplikaci</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../ui/settings.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="46"/>
        <source>Player 1</source>
        <translation>Hráč 1</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="77"/>
        <location filename="../ui/settings.ui" line="172"/>
        <location filename="../ui/settings.ui" line="176"/>
        <source>Human</source>
        <translation>Člověk</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="82"/>
        <location filename="../ui/settings.ui" line="181"/>
        <source>AI - Easy</source>
        <translation>UI - Snadný</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="87"/>
        <location filename="../ui/settings.ui" line="186"/>
        <source>AI - Normal</source>
        <translation>UI - Střední</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="92"/>
        <location filename="../ui/settings.ui" line="191"/>
        <source>AI - Hard</source>
        <translation>UI - Náročný</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="116"/>
        <location filename="../ui/settings.ui" line="218"/>
        <source>Add stone</source>
        <translation>Přidat kámen</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="129"/>
        <location filename="../ui/settings.ui" line="231"/>
        <source>Edit stone</source>
        <translation>Upravit kámen</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="145"/>
        <source>Player 2</source>
        <translation>Hráč 2</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="249"/>
        <source>Background</source>
        <translation>Obrázek na pozadí</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="308"/>
        <source>Size of the board</source>
        <translation>Velikost pole</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="317"/>
        <source>Width:</source>
        <translation>Šířka:</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="324"/>
        <source>Height:</source>
        <translation>Výška:</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="389"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="419"/>
        <location filename="../ui/settings.ui" line="423"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="428"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="445"/>
        <source>Return to default</source>
        <translation>Vrátit výchozí</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="473"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="486"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../ui/settings.cpp" line="14"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../ui/settings.cpp" line="62"/>
        <source>Image Files(*.png *.jpg)</source>
        <translation>Obrazové soubory(*.png *.jpg)</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../ui/statusbar.cpp" line="78"/>
        <source>A cell is full or invalid</source>
        <translation>Políčko je plné nebo není validní</translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="79"/>
        <source>Player 1</source>
        <translation>Hráč 1</translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="80"/>
        <source>Player 2</source>
        <translation>Hráč 2</translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="81"/>
        <source> is on the turn</source>
        <translation> je na tahu</translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="82"/>
        <source>Score: </source>
        <translation>Skóre: </translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="83"/>
        <source>Move: </source>
        <translation>Tah: </translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="84"/>
        <source> wins!</source>
        <translation> vítězí!</translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="85"/>
        <source>It&apos;s a draw!</source>
        <translation>Hra skončila nerozhodně!</translation>
    </message>
</context>
</TS>
