<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../ui/about.ui" line="32"/>
        <source>About Piskvorky</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/about.ui" line="57"/>
        <source>Piskvorky 2.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/about.ui" line="87"/>
        <source>Description</source>
        <translation>Piskvorky is type of a game know also as &apos;five in row&apos; or &apos;gomoku&apos;. &lt;br&gt;
This application is designed by Jan Vondráček &lt;a href=&quot;mailto:jan195@null.com&quot;&gt;email&lt;/a&gt;&lt;br&gt;
Source code on &lt;a href=&quot;https://gitlab.com/Jan195/piskvorky&quot;&gt;gitlab&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>EditStoneDialog</name>
    <message>
        <location filename="../ui/editstonedialog.ui" line="32"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.ui" line="52"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.ui" line="62"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.ui" line="85"/>
        <source>Highlight image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.ui" line="140"/>
        <source>&amp;Delete</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/editstonedialog.cpp" line="44"/>
        <source>Image Files(*.png *.jpg)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.ui" line="26"/>
        <source>Piskvorky 2.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="64"/>
        <source>&amp;Game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="71"/>
        <source>&amp;Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="82"/>
        <source>&amp;New game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="87"/>
        <source>&amp;Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="92"/>
        <source>&amp;Reset score</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="97"/>
        <source>&amp;Quit app</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="102"/>
        <source>&amp;About</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../ui/settings.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="46"/>
        <source>Player 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="77"/>
        <location filename="../ui/settings.ui" line="172"/>
        <location filename="../ui/settings.ui" line="176"/>
        <source>Human</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="82"/>
        <location filename="../ui/settings.ui" line="181"/>
        <source>AI - Easy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="87"/>
        <location filename="../ui/settings.ui" line="186"/>
        <source>AI - Normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="92"/>
        <location filename="../ui/settings.ui" line="191"/>
        <source>AI - Hard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="116"/>
        <location filename="../ui/settings.ui" line="218"/>
        <source>Add stone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="129"/>
        <location filename="../ui/settings.ui" line="231"/>
        <source>Edit stone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="145"/>
        <source>Player 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="249"/>
        <source>Background</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="308"/>
        <source>Size of the board</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="317"/>
        <source>Width:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="324"/>
        <source>Height:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="389"/>
        <source>Language</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="419"/>
        <location filename="../ui/settings.ui" line="423"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="428"/>
        <source>Czech</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="445"/>
        <source>Return to default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="473"/>
        <source>Save</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="486"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.cpp" line="14"/>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settings.cpp" line="62"/>
        <source>Image Files(*.png *.jpg)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../ui/statusbar.cpp" line="78"/>
        <source>A Cell is full or invalid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="79"/>
        <source>Player 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="80"/>
        <source>Player 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="81"/>
        <source> is on the turn</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="82"/>
        <source>Score: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="83"/>
        <source>Move: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="84"/>
        <source> wins!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/statusbar.cpp" line="85"/>
        <source>It&apos;s a draw!</source>
        <translation></translation>
    </message>
</context>
</TS>
