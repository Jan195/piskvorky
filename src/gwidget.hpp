#ifndef GWIDGET_HPP
#define GWIDGET_HPP

#include <QWidget>
#include <QPainter>
#include <QResizeEvent>

#include "board.hpp"
#include "config/imagepaths.hpp"

class GWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GWidget(QWidget *parent = nullptr)
        : QWidget(parent), observedBoard(nullptr), cellSize(0) {}
    void loadOriginImages(const ImagesPaths &);
    void setLastAddedStonePosition(bd::Point position) {
        lastStonePosition = position;
    }

    bool isWithinBoard(bd::Point point);
    bd::Point getCellPosition(bd::Point cursorPosition);

public slots:
    void setObservedBoard(const Board *board) {
        observedBoard = board;
    }

private:
    void resizeEvent(QResizeEvent *event) override;
    void paintEvent(QPaintEvent *pE) override;
    void paintGrid();
    void paintStones();
    void paintWinningLine();
    void resizeImages();
    void updateCellSize(bd::Size);
    const QImage& getStoneImage(bd::Point) const;

    QPainter painter;
    const Board *observedBoard;
    int cellSize;
    /**
     * @brief Contains all stone's images for a one player.
     */
    struct PlayerStone {
        QImage main,
            last,
            origin,
            originLast;
        void scale(int toSize);
    } player1Stone,
        player2Stone;
    QImage background,
        backgroundOrigin;
    bd::Point lastStonePosition;
};

#endif // GWIDGET_HPP
