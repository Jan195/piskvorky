#ifndef PLAYER_HPP
#define PLAYER_HPP

/**
 * @brief Distinction between players.
 */
enum class Player : char { NONE = 0,
                           PLAYER1 = 'x',
                           PLAYER2 = 'o',
                         };

#endif // PLAYER_HPP
