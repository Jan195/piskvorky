#include "stones.hpp"
#include "stonepath.hpp"

namespace
{
QString toQString(const Stone &s) {
    return s.name + " " + s.pathMain + " " + s.pathLast;
}
}

void StonesBuilder::setDefault() {
    if (!stones.empty())
        stones.clear();
    stones.push_back({"X", "img/x.png", "img/fx.png"});
    stones.push_back({"O", "img/o.png", "img/fo.png"});
}

unsigned int Stones::findStoneIndex(const StonePath &path) {
    for (unsigned int i = 0; i < size(); ++i) {
        if (operator[](i).pathMain == path.stone && operator[](i).pathLast == path.stoneLast)
            return i;
    }
    return 0;
}

void StonesBuilderFile::loadContent() {
    Stones stones;
    while (!this->atEnd()) {
        QStringList rawStone = this->readLine().split(" ");
        Stone newStone = {rawStone[0], rawStone[1], rawStone[2]};
        stones.push_back(newStone);
    }
    set(stones);
}

void StonesBuilderFile::saveContent() {
    for (const Stone &stone : get()) {
        QString line = toQString(stone) + "\n";
        *this << line;
    }
}
