#ifndef STONES_HPP
#define STONES_HPP

#include "textfile.hpp"

class StonePath;

struct Stone {
    QString name,
            pathMain,
            pathLast;
};

class Stones : public std::vector<Stone>
{
public:
    unsigned int findStoneIndex(const StonePath &);

private:
};

class StonesBuilder
{
public:
    StonesBuilder() {
        setDefault();
    }

    void setDefault();
    StonesBuilder& set(const Stones &stones) {
        this->stones = stones;
        return *this;
    }
    const Stones& get() {
        return stones;
    }

private:
    Stones stones;
};

class StonesBuilderFile : public StonesBuilder, public TextFile
{
public:
    explicit StonesBuilderFile(const QString &path = "") : TextFile(path) {}

protected:
    virtual void loadContent() override;
    virtual void saveContent() override;
};

#endif // STONECOLLECTION_HPP
