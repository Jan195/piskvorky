#ifndef SETTINGS_H
#define SETTINGS_H

#include <memory>

#include "ui/subwindow.hpp"
#include "config/gameconfig.hpp"
#include "config/imagepaths.hpp"
#include "editstonedialog.hpp"
#include "stones.hpp"
#include "player.hpp"

namespace Ui {
    class Settings;
}

class Languages;

class Settings : public SubWindow
{
    Q_OBJECT

public:
    explicit Settings(QWidget *pParent = nullptr);
    virtual ~Settings() override;

    void configure(const GameConfig&);
    void configure(const Stones&);
    void setPaths(const ImagesPaths&);
    void setCurrentLanguage(const Languages&);
    unsigned getCurrentLanguage();
    GameConfig getGameConfig();
    ImagesPaths getImagesPaths();
    Stones& getStones() {
        return stones;
    }

signals:
    void saved();
    void closed();

public slots:
    void saveChangesOfStones();

private slots:
    void on_cancel_clicked() {
        emit closed();
    }
    void on_save_clicked() {
        emit saved();
        emit closed();
    }
    void on_returnDefault_clicked();
    void on_openBgImgFile_clicked();
    void on_addStone1_clicked();
    void on_addStone2_clicked();
    void on_editStone1_clicked();
    void on_editStone2_clicked();

private:
    void addNewStone();
    void editStone(unsigned int id);
    void setStoneIndex(Player, unsigned int id);
    unsigned int getStoneIndex(Player);

    Ui::Settings *ui;
    std::unique_ptr<EditStoneDialog> stoneEdit;
    Stones stones;
};

#endif // SETTINGS_H
