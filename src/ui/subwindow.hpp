#ifndef SUBWINDOW_HPP
#define SUBWINDOW_HPP

#include <QWidget>
#include <QMouseEvent>
#include <QPainter>

#include "bidimensional.hpp"

class SubWindow : public QWidget
{
    Q_OBJECT
public:
    explicit SubWindow(QWidget *parent = nullptr);
    void setTitle(const QString &name) {
        title = name;
    }
public slots:
    void center();

private:
    bd::Size getTitlePositionIn(bd::Size area);
    void paintEvent(QPaintEvent*) override;
    void paintBorder(bd::Size);
    void paintTitleBar(bd::Size);
    void mousePressEvent(QMouseEvent*) override;
    void mouseReleaseEvent(QMouseEvent*) override {
        isDragging = false;
    }
    void mouseMoveEvent(QMouseEvent*) override;
    bool isCursorWithinParent(const QPoint&);

    QPainter painter;
    QString title;
    bd::Point cursorPressPosition;
    bool isDragging;

};

#endif // SUBWINDOW_HPP
