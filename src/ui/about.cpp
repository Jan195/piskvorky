#include "about.hpp"
#include "ui_about.h"

About::About(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::About)
{
    ui->setupUi(this);
}

About::~About() {
    delete ui;
}

void About::retranslate() {
    ui->retranslateUi(this);
}
