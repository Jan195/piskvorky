#ifndef SEDITDIALOG_H
#define SEDITDIALOG_H

#include <QDialog>

#include "stones.hpp"

namespace Ui {
class EditStoneDialog;
}

class EditStoneDialog : public QDialog
{
    Q_OBJECT

public:
    enum class State : char { DO_NOTHING, SAVE, REMOVE };
    struct Result {
        unsigned id;
        State state;
        Stone stone;
    };

    explicit EditStoneDialog(QWidget *parent = nullptr, unsigned int id = 0);
    ~EditStoneDialog();
    void setStone(Stone&);
    const Result& getResult() const {
        return result;
    }

signals:
    void closed();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_remove_clicked();
    void on_openImgFile_clicked();
    void on_openLastImgFile_clicked();

private:
    QString loadPath();
    Ui::EditStoneDialog *mUi;
    Result result;
};

#endif // SEDITDIALOG_H
