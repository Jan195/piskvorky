#ifndef STATUSBAR_HPP
#define STATUSBAR_HPP

#include <QStatusBar>
#include <QHBoxLayout>
#include <QLabel>

#include "bidimensional.hpp"
#include "gameresult.hpp"
#include "player.hpp"

class StatusBar : public QStatusBar
{
    Q_OBJECT

public:
    StatusBar();
    void setPositionUnderCursor(bd::Point pos);
    void setState(GameResult result, Player currentPlayer);
    void setScore(int p1, int p2);
    void setTurn(int turnCount, bd::Point lastPosition);
    void showCurrentPlayerState(Player player);
    void showResultState(GameResult result);
    void showWarning();
    void updateTranslation();

private:
    QWidget rightPart;
    QHBoxLayout rightPartLayout;
    QLabel state, score, turn, positionUnderCursor;
    QString scoreText, turnText, player1Text, player2Text,
            invalidTurnText, currentPlayerText, drawText, winText;
};

#endif // MAINSTATUSBAR_HPP
