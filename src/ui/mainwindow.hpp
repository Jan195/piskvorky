#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QLabel>
#include <memory>

#include "gamesession.hpp"
#include "statusbar.hpp"
#include "config/windowconfig.hpp"
#include "ui_mainwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *pParent = nullptr);
    ~MainWindow() override {
        delete ui;
    }

    void load(const WindowConfig &);
    void configureGame(const GameConfig &config) {
        game.configure(config);
    }
    void updateImages(const ImagesPaths &config) {
        ui->gwidget->loadOriginImages(config);
    }
    void saveTo(WindowConfig &config) {
        config.setSize({this->width(), this->height()});
    }
    const GameSession& getGameSession() {
        return game;
    }
    void startNewGame();
    void updateTranslation();

signals:
    void settingsOpened();
    void aboutOpened();
    void resized();

private slots:
    void updateMessageLabel();

    // UI interaction
    void on_startNew_triggered() { startNewGame(); }
    void on_resetScore_triggered();
    void on_about_triggered() { emit aboutOpened(); }
    void on_settings_triggered() { emit settingsOpened(); }
    void on_quit_triggered() { QApplication::quit(); }

private:
    void playTurn(bd::Point onPosition);
    void mousePressEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;
    void resizeEvent(QResizeEvent*) override { emit resized(); }
    bd::Point getCellPosition(bd::Point cursorPosition);

    void setupStatusBar();
    void updateStatusBar();
    void updateScoreLabel();
    void updateTurnLabel();

    Ui::MainWindow *ui;
    GameSession game;
    StatusBar statusbar;
};

#endif // MAINWINDOW_HPP
