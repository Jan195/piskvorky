#include "statusbar.hpp"

#include "player.hpp"

StatusBar::StatusBar()
{
    setSizeGripEnabled(false);
    state.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    score.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    turn.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    score.setAlignment(Qt::AlignmentFlag::AlignHCenter);
    turn.setAlignment(Qt::AlignmentFlag::AlignRight);
    positionUnderCursor.setAlignment(Qt::AlignmentFlag::AlignRight);

    positionUnderCursor.setIndent(0);
    turn.setIndent(0);

    rightPartLayout.addWidget(&turn);
    rightPartLayout.addWidget(&positionUnderCursor);
    rightPartLayout.setMargin(0);
    rightPart.setLayout(&rightPartLayout);

    addPermanentWidget(&state, 1);
    addPermanentWidget(&score, 1);
    addPermanentWidget(&rightPart, 1);
}

void StatusBar::setState(GameResult result, Player currentPlayer) {
    if (result != GameResult::NONE)
        showResultState(result);
    else
        showCurrentPlayerState(currentPlayer);
}

void StatusBar::showCurrentPlayerState(Player player) {
    state.setStyleSheet("QLabel {}");
    if (player == Player::PLAYER1)
        state.setText(player1Text + currentPlayerText);
    else
        state.setText(player2Text + currentPlayerText);

}

void StatusBar::showResultState(GameResult result) {
    state.setStyleSheet("QLabel {}");
    if (result == GameResult::PLAYER1)
        state.setText(player1Text + winText);
    else if (result == GameResult::PLAYER2)
        state.setText(player2Text + winText);
    else if (result == GameResult::DRAW)
        state.setText(drawText);
}

void StatusBar::showWarning() {
    showMessage("a", 1000);
    state.setStyleSheet("QLabel {color:red}");
    state.setText(invalidTurnText);
}

void StatusBar::setScore(int p1, int p2) {
    score.setText(scoreText + QString::number(p1) + ":" + QString::number(p2));
}

void StatusBar::setTurn(int turnCount, bd::Point lastPosition) {
    this->turn.setText(turnText + QString::number(turnCount) + " ["
                       + QString::fromStdString(bd::toString(lastPosition)) + "]");
}

void StatusBar::setPositionUnderCursor(bd::Point pos) {
    static bd::Point old;
    if (old != pos) {
        old = pos;
        positionUnderCursor.setText(QString::fromStdString(bd::toString(pos)) + " ");
    }
}

void StatusBar::updateTranslation() {
    invalidTurnText = tr("A cell is full or invalid");
    player1Text = tr("Player 1");
    player2Text = tr("Player 2");
    currentPlayerText = tr(" is on the turn");
    scoreText = tr("Score: ");
    turnText = tr("Move: ");
    winText = tr(" wins!");
    drawText = tr("It's a draw!");
}
