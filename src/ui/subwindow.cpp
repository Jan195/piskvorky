#include "subwindow.hpp"

SubWindow::SubWindow(QWidget *parent) : QWidget(parent), isDragging(false) {
}

void SubWindow::center() {
    QPoint center = {parentWidget()->geometry().width()/2, parentWidget()->geometry().height()/2};
    this->move(center - QPoint{geometry().width()/2, geometry().height()/2});
}

void SubWindow::paintEvent(QPaintEvent*) {
    painter.begin(this);
    bd::Size border{this->width() - 1, this->height() - 1};
    paintBorder(border);
    paintTitleBar(border);
    painter.end();
}

void SubWindow::paintBorder(bd::Size size) {
    QPen pen;
    pen.setColor(Qt::black);
    painter.setPen(pen);
    painter.drawLine(size.width, 0, size.width, size.height);
    painter.drawLine(size.width, size.height, 0, size.height);
    painter.drawLine(0, size.height, 0, 0);
}

void SubWindow::paintTitleBar(bd::Size size) {
    QPen pen;
    painter.fillRect(0, 0, size.width, 20, Qt::black);
    pen.setColor(Qt::white);
    painter.setPen(pen);
    bd::Size titlePosition = getTitlePositionIn(size);
    painter.drawText(titlePosition.width, titlePosition.height, title);
}

bd::Size SubWindow::getTitlePositionIn(bd::Size area) {
    return {area.width/2 - painter.fontMetrics().horizontalAdvance(title)/2,
            painter.fontMetrics().height()};
}

void SubWindow::mousePressEvent(QMouseEvent *cursor) {
    if (cursor->x() <= 280 && cursor->y() <= 20) isDragging = true;
    cursorPressPosition = {cursor->x(), cursor->y()};
}

void SubWindow::mouseMoveEvent(QMouseEvent *cursor) {
    if (isDragging && isCursorWithinParent(cursor->windowPos().toPoint()))
        this->move(cursor->windowPos().x() - cursorPressPosition.x,
                   cursor->windowPos().y() - cursorPressPosition.y);
}

bool SubWindow::isCursorWithinParent(const QPoint &point) {
    int border = 5;
    if ((point.x() > border && point.y() > border)
        && point.x() < this->parentWidget()->width() - border
        && point.y() < this->parentWidget()->height() - border)
        return true;
    else return false;
}
