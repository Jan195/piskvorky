#include <QFileDialog>

#include "editstonedialog.hpp"
#include "ui_editstonedialog.h"

EditStoneDialog::EditStoneDialog(QWidget *parent, unsigned int id) :
    QDialog(parent),
    mUi(new Ui::EditStoneDialog)
{
    result.id = id;
    mUi->setupUi(this);
}

EditStoneDialog::~EditStoneDialog() {
    delete mUi;
}

void EditStoneDialog::setStone(Stone &stone) {
    mUi->editName->setText(stone.name);
    mUi->editImgPath->setText(stone.pathMain);
    mUi->editLastImgPath->setText(stone.pathLast);
}

void EditStoneDialog::on_buttonBox_accepted() {
    result.state = State::SAVE;
    result.stone = {mUi->editName->text(), mUi->editImgPath->text(), mUi->editLastImgPath->text()};
    emit closed();
}

void EditStoneDialog::on_remove_clicked() {
    result.state = State::REMOVE;
    emit closed();
}

void EditStoneDialog::on_buttonBox_rejected() {
    result.state = State::DO_NOTHING;
    emit closed();
}

QString EditStoneDialog::loadPath() {
    QDir currentDir;
    QString path = QFileDialog::getOpenFileName(this, "", "data/img/", tr("Image Files(*.png *.jpg)"));
    return currentDir.relativeFilePath(path);
}

void EditStoneDialog::on_openImgFile_clicked() {
    QString newPath = loadPath();
    if (!newPath.isEmpty())
        mUi->editImgPath->setText(newPath);
}

void EditStoneDialog::on_openLastImgFile_clicked() {
    QString newPath = loadPath();
    if (!newPath.isEmpty())
        mUi->editLastImgPath->setText(newPath);
}
