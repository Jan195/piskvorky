#include <QFileDialog>

#include "settings.hpp"
#include "ui_settings.h"
#include "config/languages.hpp"

Settings::Settings(QWidget *parent) :
    SubWindow(parent), ui(new Ui::Settings)
{
    ui->setupUi(this);
    center();
    setTitle(tr("Settings"));
}

Settings::~Settings() {
    delete ui;
}

void Settings::on_returnDefault_clicked() {
    configure(GameConfigBuilder().setDefault().get());
    ui->stonePlayer1->setCurrentIndex(0);
    ui->stonePlayer2->setCurrentIndex(1);
    ui->bgImgPath->setText("");
    ui->language->setCurrentIndex(0);
}

void Settings::on_openBgImgFile_clicked() {
    QDir currentDir;
    QString imgPath = QFileDialog::getOpenFileName(this, "", "data/img/background/",
                                             tr("Image Files(*.png *.jpg)"));
    ui->bgImgPath->setText(currentDir.relativeFilePath(imgPath));
}

void Settings::setCurrentLanguage(const Languages &langs) {
    ui->language->setCurrentIndex(langs.getCurrentId());
}

unsigned Settings::getCurrentLanguage() {
    return ui->language->currentIndex();
}

void Settings::configure(const GameConfig &config) {
    ui->editWidth->setValue(config.boardSize.width);
    ui->editHeight->setValue(config.boardSize.height);
    ui->typePlayer1->setCurrentIndex(config.playerTypes.first);
    ui->typePlayer2->setCurrentIndex(config.playerTypes.second);

    ui->typePlayer1->setItemData(3, 0, Qt::UserRole - 1);
    ui->typePlayer2->setItemData(3, 0, Qt::UserRole - 1);
}

void Settings::configure(const Stones &stones) {
    this->stones = stones;
    ui->stonePlayer1->clear();
    ui->stonePlayer2->clear();
    for (const auto &stone : stones) {
        ui->stonePlayer1->addItem(QIcon(stone.pathMain), stone.name);
        ui->stonePlayer2->addItem(QIcon(stone.pathMain), stone.name);
    }
}

GameConfig Settings::getGameConfig() {
    GameConfig config;
    config.boardSize = {ui->editWidth->value(), ui->editHeight->value()};
    config.playerTypes = {ui->typePlayer1->currentIndex(),
                          ui->typePlayer2->currentIndex()};
    return config;
}

void Settings::setPaths(const ImagesPaths &config) {
    ui->bgImgPath->setText(config.backgroundPath);
    setStoneIndex(Player::PLAYER1, stones.findStoneIndex(config.stone1));
    setStoneIndex(Player::PLAYER2, stones.findStoneIndex(config.stone2));
}

ImagesPaths Settings::getImagesPaths() {
    ImagesPaths config;
    unsigned idPlayer1 = ui->stonePlayer1->currentIndex(),
            idPlayer2 = ui->stonePlayer2->currentIndex();
    config.stone1 = {stones[idPlayer1].pathMain, stones[idPlayer1].pathLast};
    config.stone2 = {stones[idPlayer2].pathMain, stones[idPlayer2].pathLast};
    config.backgroundPath = ui->bgImgPath->text();
    return config;
}

void Settings::on_addStone1_clicked() {
    addNewStone();
    setStoneIndex(Player::PLAYER1, stones.size() - 1);
    editStone(getStoneIndex(Player::PLAYER1));
}

void Settings::on_addStone2_clicked() {
    addNewStone();
    setStoneIndex(Player::PLAYER2, stones.size() - 1);
    editStone(getStoneIndex(Player::PLAYER2));
}

void Settings::on_editStone1_clicked() {
    editStone(getStoneIndex(Player::PLAYER1));
}

void Settings::on_editStone2_clicked() {
    editStone(getStoneIndex(Player::PLAYER2));
}

unsigned int Settings::getStoneIndex(Player player) {
    if (player == Player::PLAYER1)
        return ui->stonePlayer1->currentIndex();
    else
        return ui->stonePlayer2->currentIndex();
}

void Settings::setStoneIndex(Player player, unsigned int id) {
    if (player == Player::PLAYER1)
        ui->stonePlayer1->setCurrentIndex(id);
    else
        ui->stonePlayer2->setCurrentIndex(id);
}

void Settings::addNewStone() {
    stones.push_back(Stone());
    ui->stonePlayer1->addItem("");
    ui->stonePlayer2->addItem("");
}

void Settings::editStone(unsigned int id) {
    stoneEdit.reset(new EditStoneDialog(this, id));
    stoneEdit->setVisible(true);
    connect(stoneEdit.get(), &EditStoneDialog::closed, this, &Settings::saveChangesOfStones);
    stoneEdit->setWindowTitle(ui->editStone1->text() + " " + QString::number(id + 1));
    stoneEdit->setStone(stones[id]);
}

void Settings::saveChangesOfStones() {
    const EditStoneDialog::Result &result = stoneEdit->getResult();
    if (result.state == EditStoneDialog::State::SAVE) {
        stones[result.id] = result.stone;
        ui->stonePlayer1->setItemText(result.id, stones[result.id].name);
        ui->stonePlayer1->setItemIcon(result.id, QIcon(stones[result.id].pathMain));
        ui->stonePlayer2->setItemText(result.id, stones[result.id].name);
        ui->stonePlayer2->setItemIcon(result.id, QIcon(stones[result.id].pathMain));
    }
    else if (result.state == EditStoneDialog::State::REMOVE) {
        stones.erase(stones.begin() + result.id);
        ui->stonePlayer1->removeItem(result.id);
        ui->stonePlayer2->removeItem(result.id);
    }
    stoneEdit.reset();
}
