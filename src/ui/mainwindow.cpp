#include "mainwindow.hpp"

namespace {
int MIN_SIZE_FOR_CELL = 28;
}

MainWindow::MainWindow(QWidget *pParent) :
    QMainWindow(pParent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(&game, &GameSession::boardChanged, ui->gwidget, &GWidget::setObservedBoard);
    emit game.boardChanged(game.getBoard());
    setupStatusBar();
    setMouseTracking(true);
    ui->centralWidget->setMouseTracking(true);
    ui->gwidget->setMouseTracking(true);
}

void MainWindow::setupStatusBar() {
    setStatusBar(&statusbar);
    connect(statusBar(), &QStatusBar::messageChanged, this, &MainWindow::updateMessageLabel);
}

void MainWindow::load(const WindowConfig &config) {
    this->resize(config.getSize().width, config.getSize().height);
    this->setMinimumSize(QSize(game.getBoard()->getSize().width * MIN_SIZE_FOR_CELL,
                               game.getBoard()->getSize().height * MIN_SIZE_FOR_CELL));
}

void MainWindow::startNewGame() {
    game.startNew();
    updateStatusBar();
    ui->gwidget->update();
}

void MainWindow::playTurn(bd::Point onPosition) {
    game.play(onPosition);
    ui->gwidget->setLastAddedStonePosition(game.getLastPosition());
    ui->gwidget->update();
    updateStatusBar();
}

void MainWindow::mousePressEvent(QMouseEvent *event) {
    try {
        playTurn(getCellPosition({event->x(), event->y()}));
    } catch (invalid_turn) {
        statusbar.showWarning();
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event) {
    if (ui->gwidget->isWithinBoard({event->x(), event->y()}))
        statusbar.setPositionUnderCursor(getCellPosition({event->x(), event->y()}));
}

bd::Point MainWindow::getCellPosition(bd::Point cursorPosition) {
    cursorPosition.y -= ui->mainMenu->height();
    return ui->gwidget->getCellPosition(cursorPosition);
}

void MainWindow::updateStatusBar() {
    updateMessageLabel();
    updateTurnLabel();
    updateScoreLabel();
}

void MainWindow::updateMessageLabel() {
    statusbar.setState(game.getResult(), game.getCurrentPlayer());
}

void MainWindow::updateTurnLabel() {
    statusbar.setTurn(game.getTurnCount(), game.getLastPosition());
}

void MainWindow::updateScoreLabel() {
    statusbar.setScore(game.getScore().player1, game.getScore().player2);
}

void MainWindow::updateTranslation() {
    statusbar.updateTranslation();
    ui->retranslateUi(this);
    updateStatusBar();
}

void MainWindow::on_resetScore_triggered() {
    game.resetScores();
    updateScoreLabel();
}
