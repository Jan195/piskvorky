#ifndef BIDIMENSIONAL_HPP
#define BIDIMENSIONAL_HPP

#include <string>

/**
 * @brief Bi-Dimensional namespace contains few 2D entities
 */
namespace bd {

struct Size;

struct Point {
    Point() : Point(0, 0) {}
    Point(int x, int y) : x(x), y(y) {}
    Point(const Size &);

    Point& operator+=(const Point &);
    Point& operator-=(const Point &);
    Point& operator*=(int);
    Point& operator/=(int);

    int x,
        y;
};

struct Size {
    Size() : Size(0, 0) {}
    Size(int width, int height) : width(width), height(height) {}
    Size(const Point &p) {
        width = p.x;
        height = p.y;
    }

    Size& operator+=(const Size &);
    Size& operator-=(const Size &);
    Size& operator*=(int);
    Size& operator/=(int);

    bool isWithin(bd::Point);

    int width,
        height;
};

struct Line {
    Line& operator+=(const Line &);
    Line& operator-=(const Line &);
    Line& operator*=(int);
    Line& operator/=(int);

    Point start{0,0},
        end{0,0};
};

inline const Point operator+(const Point &p1, const Point &p2) {
    return {p1.x + p2.x, p1.y + p2.y};
}
inline const Point operator-(const Point &p1, const Point &p2) {
    return {p1.x - p2.x, p1.y - p2.y};
}
inline const Point operator-(const Point &p) {
    return {-p.x, -p.y};
}
inline const Point operator*(const Point &p, const int factor) {
    return {p.x * factor, p.y * factor};
}
inline const Point operator/(const Point &p, int divisor) {
    return {p.x / divisor, p.y / divisor};
}

inline const Line operator+(const Line &line1, const Line &line2) {
    return {line1.start + line2.start, line1.end + line2.end};
}
inline const Line operator+(const Line &line, int add) {
    return {line.start + Point{add,add}, line.end + Point{add, add}};
}
inline const Line operator-(const Line &line1, const Line &line2) {
    return {line1.start - line2.start, line1.end - line2.end};
}
inline const Line operator-(const Line &line, int sub) {
    return {line.start - Point{sub,sub}, line.end - Point{sub,sub}};
}
inline const Line operator-(const Line &line) {
    return {-line.start, -line.end};
}
inline const Line operator*(const Line &line, int factor) {
    return {line.start * factor, line.end * factor};
}
inline const Line operator/(const Line &line, int divisor) {
    return {line.start / divisor, line.end / divisor};
}

bool operator==(const Point &, const Point &);
bool operator!=(const Point &, const Point &);

bool operator==(const Line &, const Line &);
bool operator!=(const Line &, const Line &);

bool isNull(const Point &);
bool isNull(const Line &);

bool isValid(const Line &);

inline std::string toString(const Point p) {
    return std::to_string(p.x) + ":" + std::to_string(p.y);
}

} // end of namespace bd

#endif // BIDIMENSIONAL_HPP
