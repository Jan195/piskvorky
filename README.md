# piskvorky
Piskvorky is 'five in a row' (also know as 'Gomoku') game. Small project provides customization and offer basic AI.
Written in C++ with Qt framework.

![image1](http://jan195.8u.cz/img/piskvorky1.png)
![image2](http://jan195.8u.cz/img/piskvorky2.png)
![image3](http://jan195.8u.cz/img/piskvorky3.png)
![image4](http://jan195.8u.cz/img/piskvorky4.png)
![image5](http://jan195.8u.cz/img/piskvorky5.png)